# -*- coding: utf-8 -*-
# Script: DoubleRobot Master Workflow
#
# PLEASE NOTE that this file is part of the GOM Inspect Professional software
# You are not allowed to distribute this file to a third party without written notice.
#
# Please, do not copy and/or modify this script.
# All modifications of DoubleRobotCell should happen in the DoubleRobotExtension script.
# Ignoring this advice will make DoubleRobotCell fail after Software update.
#
# Copyright (c) 2016 GOM GmbH
# Author: GOM Software Development Team (M.V.)
# All rights reserved.

# GOM-Script-Version: 7.7
#



from KioskInterface.Base import Workflow, Evaluate, Dialogs
from KioskInterface.Base.Misc import Utils, Globals, LogClass
from KioskInterface.Base.Communication import Communicate, AsyncClient
from KioskInterface.Base.Measuring import Measure

from DoubleRobotCell.Base import DoubleRobotCommunicate, DoubleRobotUtils, DoubleRobotMeasure, DoubleRobotEvaluate
from DoubleRobotCell import DoubleRobotExtension

import asyncore, asynchat, socket
import gom
import os
import time
import datetime
import glob
import sys

""" Änderungen von TM zur Hebersteuerung.
	2017-01-03, Imports markiert in:
		# measure all choosen measurement series
		und
		# verify project
"""

import init
import heber
import multiprocessing

class ClientTodos( Utils.GenericLogClass ):
	'''
	todo class, keeps track of active tasks
	'''
	def __init__( self, logger ):
		Utils.GenericLogClass.__init__( self, logger )
		self.todos = []

	def append_todo( self, signal ):
		'''
		append given signal as todo
		'''
		self.todos.append( ( signal, ) )

	def finish( self, signal ):
		'''
		remove and return given signal (on success), if found
		'''
		if signal == DoubleRobotCommunicate.SIGNAL_SUCCESS:
			id = int( signal.value )
			for i in range( len( self.todos ) ):
				if self.todos[i][0].key == id:
					self.log.info( 'finished job {}'.format( self.todos[i] ) )
					return self.todos.pop( i )
		return None

	def get_todo( self, signal ):
		'''
		remove and return given signal, returns None if not found
		'''
		if signal == DoubleRobotCommunicate.SIGNAL_SUCCESS or signal == DoubleRobotCommunicate.SIGNAL_FAILURE:
			values = signal.value.split( b'-', 1 )
			id = int( values[0] )
			for i in range( len( self.todos ) ):
				if self.todos[i][0].key == id:
					return self.todos.pop( i )
		return None

	def has_todo( self, signal ):
		'''
		check if given signal is already in the queue
		'''
		key = signal.key
		for i in range( len( self.todos ) ):
			if self.todos[i][0].key == key:
				return True
		return False


class DoubleRobotClient( AsyncClient.Client, Utils.GenericLogClass ):
	'''
	double robot implementation of the async client
	'''
	def __init__( self, logger, parent, host = 'localhost', port = 8081, sctmap = {} ):
		Utils.GenericLogClass.__init__( self, logger )
		self.parent = parent
		self.host = host
		self.port = port
		self.sctmap = sctmap
		asyncore.dispatcher.__init__( self, map = sctmap )
		self.create_socket( socket.AF_INET, socket.SOCK_STREAM )
		self.connect( ( host, port ) )
		self.handler = None


	def get_signals( self ):
		while self.check_for_activity():  # get all signals
			pass
		if self.handler is not None:
			self.connected = self.handler.connected

	def disconnect_cleanup( self ):
		del self.handler
		self.handler = None
		self.connected = False

	def still_alive( self ):
		'''
		check if still connection alive, can raise ConnectionLost
		'''
		self.get_signals()
		if self.handler is not None:
			if not self.handler.connected:
				self.log.error( 'lost connection' )
				self.disconnect_cleanup()
				raise DoubleRobotUtils.ConnectionLost( 'lost' )

	def handle_connect( self ):
		'''
		called during connection creates the communication handler and sends handshake signal
		'''
		self.log.info( 'connected' )
		self.handler = Communicate.ChatHandlerClient( self.baselog, self.socket, self.sctmap, self )
		self.handler.handshaked = False
		ownpid = os.getpid()
		self.handler.push( Communicate.Signal( Communicate.SIGNAL_HANDSHAKE, str( ownpid ) ).encode() )
		self.log.debug( 'connected and Handshake sent' )

	def handle_close( self ):
		'''
		called during close event
		'''
		if self.connected:  # only log if connection was established
			self.log.debug( 'closing' )
		self.close()

	def check_first_connection( self ):
		'''
		check for successfull first connection(with handshake pkt)
		'''
		if not self.connected:
			if self.handler is not None:
				self.handler.handshaked = False
			try:
				self.close()
			except Globals.EXIT_EXCEPTIONS:
				raise
			except:
				pass
			try:
				self.connect( ( self.host, self.port ) )
			except Globals.EXIT_EXCEPTIONS:
				raise
			except:
				pass
		if self.handler is not None and self.handler.handshaked:
			return True

	def check_for_activity( self, timeout = 0.1 ):
		'''
		checks for network packets and precesses them
		returns True if any packet was received
		'''
		if not self.connected:
			try:
				self.close()
			except Globals.EXIT_EXCEPTIONS:
				raise
			except:
				pass
			asyncore.dispatcher.__init__( self, map = self.sctmap )
			self.create_socket( socket.AF_INET, socket.SOCK_STREAM )
			self.connect( ( self.host, self.port ) )
			timeout = 3
		asyncore.loop( timeout = timeout, map = self.sctmap, count = 1 )
		if self.handler is not None:
			self.connected = self.handler.connected
			return self.handler.process_signals()
		else:
			return False
	@property
	def LastAsyncResults( self ):
		if self.connected:
			if self.handler is not None:
				for result in self.handler.LastAsyncResults:
					yield result


class DoubleRobotWorkflow( Utils.GenericLogClass ):
	def __init__( self ):
		'''
		Initializes function to init logging and subclasses
		'''
		Utils.GenericLogClass.__init__( self, LogClass.Logger() )
		self.baselog.log.setLevel( Globals.SETTINGS.LoggingLevel )
		self.logformat = Globals.SETTINGS.LoggingFormat
		self._create_fileloghandler()
		self.consolelog = self.baselog.create_console_streamhandler( strformat = self.logformat )
		Globals.registerGlobalLogger( self.baselog )
		self.Client = DoubleRobotClient( self.baselog, self, Globals.SETTINGS.HostAddress, Globals.SETTINGS.HostPort, {} )
		self.ClientTodo = ClientTodos( self.baselog )
		self.WorkflowState = ServerWorkflowState.Idle
		self.WorkflowDialog = DoubleRobotWorkflowDialog( self.baselog, self )
		self.ChooseMeasurementDialog = DoubleRobotChooseMeasurements( self.baselog, self )
		self.Evaluate = DoubleRobotEvaluate.DoubleRobotEvaluate( self.baselog, self )
		self.connected = False
		self.to_import = []
		Globals.DIALOGS.localize_temperature_dialog()

	def still_alive( self ):
		self.Client.still_alive()

	def _create_fileloghandler( self ):
		'''
		This function creates a new log file
		'''
		logdir = os.path.normpath( os.path.join( gom.app.get ( 'local_all_directory' ), '..', 'log' ) )
		filepath = os.path.join( logdir, 'doublerobot_log' + time.strftime( Globals.SETTINGS.TimeFormatLogging ) + '.log' )
		self.fileloghandler = self.baselog.create_filehandler( filename = filepath, strformat = self.logformat )
		self.remove_old_logs( os.path.join( logdir, 'doublerobot_log*.log' ) )
	def remove_old_logs( self, log_files ):
		'''
		removes all logfiles older then 4 weeks
		'''
		today = datetime.datetime.today()
		mindiff = datetime.timedelta( weeks = 4 )
		for file in glob.glob( log_files ):
			try:
				mod_time = datetime.datetime.fromtimestamp( os.path.getmtime( file ) )
				if ( abs( today - mod_time ) > mindiff ):
					os.remove( file )
			except:
				pass

	def process_signals( self ):
		'''
		process signals
		'''
		while self.Client.check_for_activity():
			pass
		for last_result in self.Client.LastAsyncResults:
			self.log.debug( 'pop result {}'.format( last_result ) )
			# client send success
			if last_result == DoubleRobotCommunicate.SIGNAL_SUCCESS:
				last_todo = self.ClientTodo.finish( last_result )  # get signal from todo list
			# client send failure
			elif last_result == DoubleRobotCommunicate.SIGNAL_FAILURE:
				last_todo = self.ClientTodo.get_todo( last_result )  # get signal from todo list
				self.log.error( 'client failure {}'.format( last_todo ) )  # and show error msg
				self.log.error( ' client msg: {}'.format( last_result.get_value_as_string() ) )
				Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title,
											Globals.LOCALIZATION.msg_DC_client_failure.format( last_result.get_value_as_string() ),
											Globals.SETTINGS.SavePath, False )
				return False
			elif last_result == Communicate.SIGNAL_EXIT:
				raise gom.BreakError
			elif last_result == DoubleRobotCommunicate.SIGNAL_EXPORTEDFILE:
				self.on_exported_file( last_result.get_value_as_string() )  # store file string
		self.Client.still_alive()
		return True

	def start( self ):
		'''
		main evaluation
		'''
		gom.script.sys.set_kiosk_status_bar(show=True)
		gom.script.sys.set_kiosk_status_bar(states=['home','measurement','evaluation'])
		gom.script.sys.set_kiosk_status_bar(status=1)
		if not os.path.exists( Globals.SETTINGS.DoubleRobot_TransferPath ):
			os.makedirs( Globals.SETTINGS.DoubleRobot_TransferPath )
		_refpath = os.path.dirname( os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_ReferencePointFileName ) )
		if not os.path.exists( _refpath ):
			os.makedirs( _refpath )
		if not os.path.exists( os.path.join( Globals.SETTINGS.SavePath, Globals.SETTINGS.DoubleRobot_AtosXMLPath ) ):
			os.makedirs( os.path.join( Globals.SETTINGS.SavePath, Globals.SETTINGS.DoubleRobot_AtosXMLPath ) )

		os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_ReferencePointFileName )
		while len( self.ClientTodo.todos ):  # wait for older pkts
			if not self.process_signals():
				return False
			gom.script.sys.delay_script( time = 2 )
		gom.script.sys.close_project ()

		self.WorkflowState = ServerWorkflowState.Connecting
		try:
			res = self.WorkflowDialog.start()
		except ( DoubleRobotUtils.ConnectionLost, DoubleRobotUtils.ExitSignal ):
			self.log.error( 'lost connection to client' )
			Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_client_exit, Globals.SETTINGS.SavePath, False )
			return False
		return res
			
	def on_exported_file(self, filepath):
		path = os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, filepath )
		if os.path.exists( path ):
			self.to_import.append( path )

	def import_exported_files( self ):
		'''
		import all stored files
		'''
		for f in self.to_import:
			gom.script.sys.import_project ( file = f, import_mode='replace_elements')
		for f in self.to_import:
			try:
				os.unlink( f )
			except:
				pass
		self.to_import = []


	def open_template( self ):
		'''
		ask user for template, checks if slave project exists and signals the template
		'''
		self.to_import = []
		gom.script.sys.close_project ()
		# which template should be used
		try:
			master_template = gom.interactive.sys.get_visible_project_templates ( 
				config_levels = ['shared'])
		except:
			return False
		client_template = DoubleRobotUtils.left_right_replace( master_template['template_name'] )

		client_template_path = os.path.join( gom.app.get ( 'public_directory' ), 'gom_project_templates', client_template.replace( chr( 0x7 ), '/' ) )
		if not os.path.exists( client_template_path ):
			self.log.error( 'no slave template' )
			Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_failed_template, Globals.SETTINGS.SavePath, False )
			return False
		# start client project
		signal = Communicate.Signal( DoubleRobotCommunicate.SIGNAL_OPEN, client_template )
		self.ClientTodo.append_todo( signal )
		self.Client.send_signal( signal )
		try:
			template = gom.script.sys.create_project_from_template ( 
					config_level = master_template['config_level'],
					template_name = master_template['template_name'] )
		except:
			self.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
			return False
		return True

	def exit_handler( self ):
		'''
		This function is called directly before the script stops (SystemExit, BreakError)
		'''
		self.log.info( 'exit handler called' )
		try:
			self.Client.send_signal( DoubleRobotCommunicate.SIGNAL_EXIT )
		except:
			pass
		self.Client.close()
		self.baselog.close_filehandle( self.fileloghandler )



class DoubleRobotChooseMeasurements( Utils.GenericLogClass ):
	'''
	Dialog class for measurement series choose
	'''
	def __init__( self, logger, parent ):
		Utils.GenericLogClass.__init__( self, logger )
		self.parent = parent
		self.dialog = None
	
	def select_measurements( self ):
		'''
		build and show dialog
		returns list of measurement series names and projectname string
		'''
		dialog = Globals.DIALOGS.MEASUREMENTLISTDIALOGTITLE
		sensor_instance = self.parent.Evaluate.Sensor
		compatible_wcfgs = sorted([wcfg.name for wcfg in gom.app.project.measuring_setups 
												if sensor_instance.same_sensor(wcfg) and sensor_instance.correct_controller(wcfg)])
		
		series = [serie for serie in gom.app.project.measurement_series.filter('type != "calibration_measurement_series" and type != "vmr_home_path_series"')
							if serie.measuring_setup.name in compatible_wcfgs]
		
		if not len(series):
			self.parent.Evaluate.showErrorNoCompatibleSeries()
			return [None,None]

		if not gom.app.get( 'kiosk_mode' ):  # one row less in kiosk mode
			dialog = dialog.format( len( series ) + 4 )
		else:
			dialog = dialog.format( len( series ) + 3 )
		row = 1
		for m in series:
			dialog += Globals.DIALOGS.MEASUREMENTLISTDIALOGCHECK.format( row, m.get( 'name' ) )
			row += 1

		dialog += Globals.DIALOGS.MEASUREMENTLISTDIALOGFOOTER.format( row, row + 1 )
		if not gom.app.get( 'kiosk_mode' ):  # dont show the wait button inside the kiosk mode
			dialog += Globals.DIALOGS.MEASUREMENTLISTDIALOGWAIT.format( row + 2 )
		dialog += Globals.DIALOGS.MEASUREMENTLISTDIALOGEND

		self.dialog = gom.script.sys.create_user_defined_dialog( content = dialog )
		self.dialog.title = Globals.LOCALIZATION.dialog_DC_ms_title
		if Globals.DIALOGS.has_widget( self.dialog, 'label_title' ):
			self.dialog.label_title.text = Globals.LOCALIZATION.dialog_DC_ms_title
		if Globals.DIALOGS.has_widget( self.dialog, 'separatorprojprefix' ):
			self.dialog.separatorprojprefix.title = Globals.LOCALIZATION.dialog_DC_project_prefix
		if Globals.DIALOGS.has_widget( self.dialog, 'buttonWait' ):
			self.dialog.buttonWait.text = Globals.LOCALIZATION.dialog_DC_wait_button
		self.dialog.handler = self.handler_measurementlist
		self.dialog.control.ok.enabled = False
		self.dialog.control.ok.text = 'Start'
		self.dialog.control.status = Globals.LOCALIZATION.status_DC_waiting
		self.dialog.timer.enabled = True
		self.dialog.timer.interval = 5000
		try:
			result = gom.script.sys.show_user_defined_dialog( dialog = self.dialog )
			return [[widget.title for widget in self.dialog.widgets if widget.name.startswith( 'checkbox' ) and widget.value], self.dialog.input_projectname.value]
		except Exception as e:
			self.log.exception( '{}'.format( e ) )
			return [None, None]

	def handler_measurementlist( self, widget ):
		'''
		dialog handler
		'''
		if Globals.DIALOGS.has_widget( self.dialog, 'buttonWait' ) and widget == self.dialog.buttonWait:
			if not self.parent.ClientTodo.has_todo( DoubleRobotCommunicate.SIGNAL_PAUSE ):
				signal = Communicate.Signal( DoubleRobotCommunicate.SIGNAL_PAUSE, 'WAITING' )
				self.parent.Client.send_signal( signal )
				self.parent.ClientTodo.append_todo( signal )
				return
		elif widget == 'timer':
			if not self.parent.process_signals():
				raise gom.BreakError
		elif widget == 'initialize':
			# test if naming rules match
			for widget in self.dialog.widgets:
				if not widget.name.startswith( 'checkbox' ):
					continue
				if DoubleRobotUtils.left_right_replace( widget.title ) == widget.title:
					widget.enabled = False
					widget.tooltip = Globals.LOCALIZATION.msg_DC_no_slave_mlist_found
				try:
					gom.app.project.measurement_series[DoubleRobotUtils.left_right_replace( widget.title )]
				except:
					widget.enabled = False
					widget.tooltip = Globals.LOCALIZATION.msg_DC_no_slave_mlist_found

		elif not isinstance( widget, str ) and widget.name.startswith( 'checkbox' ):  # ms series check changed
			pass

		if len( self.parent.ClientTodo.todos ):  # wait for open/pause
			self.dialog.control.ok.enabled = False
			self.dialog.control.status = Globals.LOCALIZATION.status_DC_waiting
		elif not any( True for widget in self.dialog.widgets if widget.name.startswith( 'checkbox' ) and widget.value ):
			self.dialog.control.status = Globals.LOCALIZATION.status_DC_choose_measurements
			self.dialog.control.ok.enabled = False
		else:
			self.dialog.control.ok.enabled = True
			self.dialog.control.status = ''


class ServerWorkflowState( Utils.EnumStructure ):
	'''
	Enum structure for workflow state
	'''
	Idle = 0
	Connecting = 1
	OpenTemplate = 2
	SelectMeasurement = 3
	SaveProject = 4
	Measure = 5
	ImportMeasurements = 6
	Verify = 7
	Exit = 8

class MeasureType( Utils.EnumStructure ):
	'''
	enum structure for measurement type
	'''
	Unknown = 0
	Atos = 1
	Tritop = 2


class DoubleRobotWorkflowDialog( Utils.GenericLogClass ):
	'''
	dialog and main workflow class
	'''
	def __init__( self, logger, parent ):
		Utils.GenericLogClass.__init__( self, logger )
		self.parent = parent
		self.dialog = Globals.DIALOGS.DOUBLE_MEASUREDIALOG
		self.dialog.handler = self.dialog_handler
		self.dialog.timer.enabled = True
		self.dialog.timer.interval = 2000
		self.dialog.control.ok.enabled = False
		self.status_change()
		self.measurements = None
		self.measure_type = MeasureType.Unknown
		self.tritop_measurements=[]
		self.atos_measurements=[]
		self.step = 1
		self.maxstep = None
		self.debug_last_state = ServerWorkflowState.Exit

	def start( self ):
		'''
		main function
		'''
		self.status_change()
		while True:
			if self.parent.WorkflowState == ServerWorkflowState.Idle:  # last state of successfull evaluation
				self.parent.WorkflowState = ServerWorkflowState.Connecting  # switch to initial state
			self.status_change()
			self.measurements = None
			self.tritop_measurements=[]
			self.atos_measurements=[]
			self.measure_type = MeasureType.Unknown
			self.dialog.control.ok.enabled = False
			try:
				res = gom.script.sys.show_user_defined_dialog( dialog = self.dialog )
				if isinstance( res, bool ) and not res:
					return False
				elif self.parent.WorkflowState == ServerWorkflowState.Connecting:  # user pressed ok during connection state
					self.parent.WorkflowState = ServerWorkflowState.OpenTemplate  # switch to next state
				elif self.parent.WorkflowState == ServerWorkflowState.SelectMeasurement:
					if not self.check_wut():
						self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
						gom.script.sys.close_project()
						self.parent.WorkflowState = ServerWorkflowState.Idle

			except ( SystemExit, gom.BreakError ):
				self.log.error( 'exit' )
				return False
			except RuntimeError as e:
				self.log.exception( 'Failure {}'.format( e ) )
				return False


	def status_change( self ):
		'''
		change status text, error text and image based on state
		'''
		waiting = ''
		if len( self.parent.ClientTodo.todos ):
			waiting = Globals.LOCALIZATION.status_DC_waiting
		if self.parent.WorkflowState == ServerWorkflowState.Connecting:
			gom.script.sys.set_kiosk_status_bar(status=1)
			self.error_msg( Globals.LOCALIZATION.status_DC_connection_wait )
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_press_ok )
		elif self.parent.WorkflowState == ServerWorkflowState.Idle:
			gom.script.sys.set_kiosk_status_bar(status=1)
			self.error_msg( waiting )
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_press_ok )
		elif self.parent.WorkflowState == ServerWorkflowState.OpenTemplate:
			gom.script.sys.set_kiosk_status_bar(status=1)
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_choose_template )
			self.error_msg( waiting )
		elif self.parent.WorkflowState == ServerWorkflowState.SelectMeasurement:
			gom.script.sys.set_kiosk_status_bar(status=1)
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_choose_measurements )
			self.error_msg( waiting )
		elif self.parent.WorkflowState == ServerWorkflowState.SaveProject:
			gom.script.sys.set_kiosk_status_bar(status=1)
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_saving )
			self.error_msg( waiting )
		elif self.parent.WorkflowState == ServerWorkflowState.Measure:
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_measuring )
			self.error_msg( waiting )
			gom.script.sys.set_kiosk_status_bar(status=2)
		elif self.parent.WorkflowState == ServerWorkflowState.ImportMeasurements:
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_importing_ginspect )
			self.error_msg( waiting )
			gom.script.sys.set_kiosk_status_bar(status=3)
		elif self.parent.WorkflowState == ServerWorkflowState.Verify:
			self.process_msg_detail( Globals.LOCALIZATION.status_DC_verify )
			self.error_msg( waiting )
			gom.script.sys.set_kiosk_status_bar(status=3)


	def dialog_handler( self, widget ):
		'''
		main dialog handler
		also main workflow function
		'''
		try:
			if widget in ['timer']:
				if self.debug_last_state != self.parent.WorkflowState:
					self.log.debug( 'state: {}'.format( self.parent.WorkflowState ) )
					self.debug_last_state = self.parent.WorkflowState
				if not self.parent.process_signals():
					self.log.error( 'failed process signals' )
					gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
					self.parent.WorkflowState = ServerWorkflowState.Idle
					return


				if not len( self.parent.ClientTodo.todos ):  # no todos, switch to next state
					if self.parent.WorkflowState == ServerWorkflowState.Connecting:
						self.processbar_max_steps( 1 )
						self.processbar_step( 1 )
						if self.parent.Client.check_first_connection():  # connection established?
							self.error_msg( '' )
							if not self.dialog.control.ok.enabled:
								self.log.debug( 'handshake successfull' )
							self.dialog.control.ok.enabled = True


					elif self.parent.WorkflowState == ServerWorkflowState.OpenTemplate:  # ask user for template
						self.error_msg( '' )
						self.processbar_max_steps( 1 )
						self.processbar_step( 1 )
						if not self.parent.open_template():
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							self.parent.WorkflowState = ServerWorkflowState.Idle
							return
						if not self.parent.Evaluate.Global_Checks.checkProjectConsistency():
							self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							self.parent.WorkflowState = ServerWorkflowState.Idle
							return
						if not self.parent.Evaluate.measurement_series_home_pos_check():
							Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_evaluate_failed_home_check_series, Globals.SETTINGS.SavePath, False )
							self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							self.parent.WorkflowState = ServerWorkflowState.Idle
							return
						if not self.parent.Evaluate.check_measuring_setups():
							Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_different_measuring_setups, Globals.SETTINGS.SavePath, False )
							self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							self.parent.WorkflowState = ServerWorkflowState.Idle
							return

						self.parent.Evaluate.display_measurement_positions( False )
						self.dialog.timer.interval = 5000
						self.parent.WorkflowState = ServerWorkflowState.SelectMeasurement
						self.status_change()
						gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = None )  # close for possible WuT check

					elif self.parent.WorkflowState == ServerWorkflowState.SelectMeasurement:  # select measurement series
						self.error_msg( '' )
						if not self.parent.Evaluate.Sensor.is_initialized():
							with Measure.TemporaryWarmupDisable(self.parent.Evaluate.Sensor) as warmup: # no warmuptime before select measurement
								self.parent.Evaluate.Sensor.initialize()
						# store measurement names and build project name
						self.measurements, project_prefix = self.parent.ChooseMeasurementDialog.select_measurements()
						if self.measurements is None or len( self.measurements ) == 0:
							self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
							gom.script.sys.close_project()
							self.parent.WorkflowState = ServerWorkflowState.Idle
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							return
						if not Globals.SETTINGS.AutoNameProject:
							if len( project_prefix ) > 0:
								project_prefix += ' '
							Globals.SETTINGS.ProjectName = project_prefix + gom.app.project.get( 'name' )
						
						#split mlists by type
						self.tritop_measurements=[]
						self.atos_measurements=[]
						for m in self.measurements:
							ml =  gom.app.project.measurement_series[m]
							if ml.type=='atos_measurement_series':
								self.atos_measurements.append(ml)
							else:
								self.tritop_measurements.append(ml)
						self.tritop_measurements = sorted(self.tritop_measurements)
						self.atos_measurements = sorted(self.atos_measurements)
						self.measurements = self.tritop_measurements + self.atos_measurements
							
						if not len(self.tritop_measurements) and len(self.atos_measurements):  # check for refxml
							if not os.path.exists( os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_ReferencePointFileName ) ):
								self.log.error( 'refxml is missing {}'.format( os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_ReferencePointFileName ) ) )
								self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
								gom.script.sys.close_project()
								self.parent.WorkflowState = ServerWorkflowState.Idle
								self.status_change()
								Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_no_xml, None, False )
								gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
								return
							self.measure_type = MeasureType.Atos
						elif len(self.tritop_measurements) and not len(self.atos_measurements): # only Tritop
							self.measure_type = MeasureType.Tritop
						else: # mixed measurements
							self.measure_type = MeasureType.Tritop
						self.parent.WorkflowState = ServerWorkflowState.SaveProject
						self.status_change()

					elif self.parent.WorkflowState == ServerWorkflowState.SaveProject:  # initial storage of project
						self.processbar_max_steps( 1 )
						self.processbar_step( 0 )
						self.error_msg( '' )
						signal = DoubleRobotCommunicate.SIGNAL_SAVE
						self.parent.ClientTodo.append_todo( signal )
						self.parent.Client.send_signal( signal )
						self.parent.Evaluate.save_project()
						self.parent.Evaluate.first_measurement_series = True
						self.parent.WorkflowState = ServerWorkflowState.Measure
						self.status_change()

					elif self.parent.WorkflowState == ServerWorkflowState.Measure:  # measure all choosen measurement series
						self.processbar_max_steps( 1 )
						self.processbar_step( 0 )
						self.error_msg( '' )
						
						measurements = []
						if self.measure_type == MeasureType.Tritop:
							measurements = self.tritop_measurements
						elif self.measure_type == MeasureType.Atos:
							measurements = self.atos_measurements
							
						""" TM Anpassung zur Heberansteuerung """
						""" Dialog """
						self.statusDIALOG=gom.script.sys.create_user_defined_dialog (content='<dialog>' \
						' <title>Achtung</title>' \
						' <style>Touch</style>' \
						' <control id="Empty"/>' \
						' <position>center</position>' \
						' <embedding></embedding>' \
						' <sizemode>automatic</sizemode>' \
						' <size height="143" width="240"/>' \
						' <content columns="4" rows="1">' \
						'  <widget type="image" column="0" rowspan="1" columnspan="1" row="0">' \
						'   <name>image</name>' \
						'   <tooltip></tooltip>' \
						'   <use_system_image>true</use_system_image>' \
						'   <system_image>system_message_warning</system_image>' \
						'   <data><![CDATA[AAAAAAAAAA==]]></data>' \
						'   <file_name></file_name>' \
						'   <keep_original_size>true</keep_original_size>' \
						'   <keep_aspect>true</keep_aspect>' \
						'   <width>0</width>' \
						'   <height>0</height>' \
						'  </widget>' \
						'  <widget type="label" column="1" rowspan="1" columnspan="3" row="0">' \
						'   <name>label</name>' \
						'   <tooltip></tooltip>' \
						'   <text>Achtung Heberbewegung</text>' \
						'   <word_wrap>false</word_wrap>' \
						'  </widget>' \
						' </content>' \
						'</dialog>')
						self.errorDIALOG=gom.script.sys.create_user_defined_dialog (content='<dialog>' \
						' <title>Achtung</title>' \
						' <style></style>' \
						' <control id="OkCancel"/>' \
						' <position></position>' \
						' <embedding></embedding>' \
						' <sizemode></sizemode>' \
						' <size width="256" height="184"/>' \
						' <content rows="2" columns="1">' \
						'  <widget row="0" rowspan="1" type="label" column="0" columnspan="1">' \
						'   <name>info</name>' \
						'   <tooltip></tooltip>' \
						'   <text>Textfeld</text>' \
						'   <word_wrap>false</word_wrap>' \
						'  </widget>' \
						'  <widget row="1" rowspan="1" type="display::text" column="0" columnspan="1">' \
						'   <name>text</name>' \
						'   <tooltip></tooltip>' \
						'   <text>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">' \
						'&lt;html>&lt;head>&lt;meta name="qrichtext" content="1" />&lt;style type="text/css">' \
						'p, li { white-space: pre-wrap; }' \
						'&lt;/style>&lt;/head>&lt;body style="    ">' \
						'&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Fehler beim Heben oder Senken!&lt;/p>' \
						'&lt;p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">&lt;br />&lt;/p>' \
						'&lt;p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">Erneut versuchen?&lt;/p>&lt;/body>&lt;/html></text>' \
						'   <wordwrap>false</wordwrap>' \
						'  </widget>' \
						' </content>' \
						'</dialog>')
						
						self.errorDIALOG.info.visible = False # Infozeile in Fehlerdialog mit Ergebniss des Multiprocess
						""" ---- """
						init.init()
						if gom.app.project.get ('user_lift') == 'up':
							self.topoHeberHeben()
					
						elif gom.app.project.get('user_lift') == 'down':
							self.topoHeberSenken()
						""" Ende der Anpassung """
						
						
						if not self.bulk_measure(measurements):
							Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_failed_measure, Globals.SETTINGS.SavePath, False )
							self.parent.Client.send_signal( DoubleRobotCommunicate.SIGNAL_FAILURE )
							gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )
							return
						
						self.parent.WorkflowState = ServerWorkflowState.ImportMeasurements
						signal = DoubleRobotCommunicate.SIGNAL_EXPORT
						self.parent.ClientTodo.append_todo( signal )
						self.parent.Client.send_signal( signal )
						DoubleRobotExtension.on_finished_measuring( self, 'atos' if self.measure_type == MeasureType.Atos else 'tritop' )
						self.status_change()

					elif self.parent.WorkflowState == ServerWorkflowState.ImportMeasurements: # import and change to verification
						self.processbar_max_steps( 2 )
						self.processbar_step( 0 )
						self.error_msg( '' )
						if len( self.parent.to_import ):
							self.parent.import_exported_files()
						self.processbar_step()
						try:
							gom.script.sys.save_project()
						except:
							pass
						self.parent.WorkflowState = ServerWorkflowState.Verify
						self.status_change()
						
					elif self.parent.WorkflowState == ServerWorkflowState.Verify:  # verify project
						self.processbar_max_steps( 3 )
						self.processbar_step( 0 )
						self.error_msg( '' )
						if self.measure_type == MeasureType.Tritop:
							# result can be True: Use, False: Abort, None:Retry
							reduced_recalc = False
							if Globals.SETTINGS.DoubleRobot_ReducedRecalc:
								if len(self.atos_measurements):
									reduced_recalc=True
							result = self.parent.Evaluate.analysis.evaluate_tritop_project( self.measurements[0],reduced_recalc )
						else:
							signal = DoubleRobotCommunicate.SIGNAL_FINISHED
							self.parent.ClientTodo.append_todo( signal )
							self.parent.Client.send_signal( signal ) # signal close project
							result = self.parent.Evaluate.analysis.evaluate_atos_project()
						self.processbar_step()
						if self.measure_type == MeasureType.Tritop:
							if result is None:
								# no measure type switch perform again tritop
								self.parent.WorkflowState = ServerWorkflowState.Measure
								self.status_change()
								return
							else:  # on abort/use signal finished
								# continue with atos only if tritop succeded
								if len(self.atos_measurements) and result == True:
									self.measure_type = MeasureType.Atos
									self.parent.WorkflowState = ServerWorkflowState.Measure
									self.status_change()
									return
								signal = DoubleRobotCommunicate.SIGNAL_FINISHED
								self.parent.ClientTodo.append_todo( signal )
								self.parent.Client.send_signal( signal ) # signal close project

						gom.script.sys.save_project ()
						
						""" TM Anpassung um den Heber abzusenken. 2017-01-04, pak """
						self.topoHeberSenken()# Anpassung zum Heber senken
						
						if result:  # on success call the extension
							DoubleRobotExtension.after_evaluation( self, 'atos' if self.measure_type == MeasureType.Atos else 'tritop' )
							gom.script.sys.close_project()
						self.parent.WorkflowState = ServerWorkflowState.Idle
						gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = True )


				else:
					self.log.info( 'waiting for finishing: {}'.format( self.parent.ClientTodo.todos ) )

		except ( DoubleRobotUtils.ConnectionLost, DoubleRobotUtils.ExitSignal ):
			self.log.error( 'lost connection to client' )
			Globals.DIALOGS.show_errormsg( Globals.LOCALIZATION.msg_general_failure_title, Globals.LOCALIZATION.msg_DC_client_exit, Globals.SETTINGS.SavePath, False )
			gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = False )
		except ( SystemExit, gom.BreakError ):
			self.log.error( 'exit' )
			gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = False )
		except RuntimeError as e:
			self.log.exception( 'Failure {}'.format( e ) )
			gom.script.sys.close_user_defined_dialog( dialog = self.dialog, result = False )
					
	def bulk_measure(self, measurement_series):

		
		# make sure that the sensor is initialized before communicate mlists 
		if not self.parent.Evaluate.Sensor.is_initialized():
			with Measure.TemporaryWarmupDisable(self.parent.Evaluate.Sensor) as warmup:
				self.parent.Evaluate.Sensor.initialize()
		client_measurements = []
		for m in measurement_series:
			client_measurements.append(DoubleRobotUtils.left_right_replace( m.name ))
		signal = Communicate.Signal( DoubleRobotCommunicate.SIGNAL_MEASURE, DoubleRobotCommunicate.MLIST_SEPARATOR.join(client_measurements) )
		self.parent.ClientTodo.append_todo( signal )
		self.parent.Client.send_signal( signal )

		for ml in measurement_series:
			res = self.parent.Evaluate.perform( ml )
			if not res:
				return False
		return True
	
	
	def topoHeberHeben(self):
		""" topometric Funktionen zum Heben des Bauteilhebers """
		success = False
		while success == False:
			gom.script.sys.open_user_defined_dialog (dialog=self.statusDIALOG)
			gom.script.sys.delay_script (time=0.1)
			q = multiprocessing.Queue()    
			hebenProzess = multiprocessing.Process(target=heber.heben, args=(q, True,))
			hebenProzess.start()
			hebenProzess.join()
			
			gom.script.sys.close_user_defined_dialog (dialog=self.statusDIALOG)
			feedback = q.get()
			if feedback == 'ACK':
				success = True
			else:
				self.errorDIALOG.info.text = feedback	
				gom.script.sys.show_user_defined_dialog (dialog=self.errorDIALOG)
	
	def topoHeberSenken(self):
		""" topometric Funktionen zum Senken des Bauteilhebers """
		success = False
		while success == False:
			gom.script.sys.open_user_defined_dialog (dialog=self.statusDIALOG)
			gom.script.sys.delay_script (time=0.1)
			q = multiprocessing.Queue()    
			senkenProzess = multiprocessing.Process(target=heber.heben, args=(q, False,))
			senkenProzess.start()
			senkenProzess.join()
			
			gom.script.sys.close_user_defined_dialog (dialog=self.statusDIALOG)
			feedback = q.get()
			if feedback == 'ACK':
				success = True
			else:
				self.errorDIALOG.info.text = feedback	
				gom.script.sys.show_user_defined_dialog (dialog=self.errorDIALOG)
				

	def check_wut( self ):
		# detect calibration series
		compatible_wcfgs = sorted([wcfg.name
			for wcfg in gom.app.project.measuring_setups 
				if self.parent.Evaluate.Sensor.same_sensor(wcfg)
					and self.parent.Evaluate.Sensor.correct_controller(wcfg)])
		comp_calib_series = [mseries for mseries in gom.app.project.measurement_series
			if mseries.get('type') == 'calibration_measurement_series'
				and self.parent.Evaluate.Sensor.calibration_ms_compatible(mseries, compatible_wcfgs)]
		self.parent.Evaluate.Calibration.MeasureList = None
		self.log.info('Compatible calibration measurement series {}'.format(comp_calib_series))
		for ms in comp_calib_series:
			self.parent.Evaluate.Calibration.MeasureList = ms
			break
		if not self.parent.Evaluate.WuT.update_temperature( self.parent.Evaluate.Calibration.MeasureList ):
			return False
		measure_temp = 0
		try:
			measure_temp = gom.app.project.get ( 'measurement_temperature' )
		except Globals.EXIT_EXCEPTIONS:
			raise
		except:
			pass
		signal = Communicate.Signal( DoubleRobotCommunicate.SIGNAL_WUT, str( measure_temp ) )
		self.parent.Client.send_signal( signal )
		return True



	@property
	def Processbar_step( self ):
		return self.dialog.progressbar.step
	@property
	def Processbar_maxstep( self ):
		return self.dialog.progressbar.parts
	@property
	def Process_msg_step( self ):
		return self.step
	@property
	def Process_msg_maxstep( self ):
		return self.maxstep
	@property
	def Process_get_msg_detail( self ):
		return self.dialog.status.text


	def process_msg( self, msg = None, step = None, maxstep = None ):
		pass

	def process_msg_detail( self, msg ):
		self.dialog.status.text = msg

	def error_msg( self, msg ):
		style = '<span style="font-style:italic;">{}</span>'
		self.dialog.error.text = style.format( msg )

	def processbar_step( self, step = None ):
		'''
		set/step the process bar
		'''
		if step is not None:
			self.dialog.progressbar.step = step
		else:
			self.dialog.progressbar.step += 1

	def processbar_max_steps( self, maxsteps = None ):
		'''
		set/step the maximum steps of the process bar
		'''
		if maxsteps is not None:
			self.dialog.progressbar.parts = maxsteps
		else:
			self.dialog.progressbar.parts += 1

	def process_image( self, img ):
		'''
		set the image of the process dialog
		'''
		pass

	def close_progressdialog( self ):
		pass
	def open_progressdialog( self ):
		pass

	def show_measuringsetup_dialog( self, series, first ):
		return False


if __name__ == '__main__':
	from DoubleRobotCell.Base import DoubleRobotLocalization
	Globals.LOCALIZATION = DoubleRobotLocalization.DoubleRobotLocalization()
	LogClass.Logger().create_console_streamhandler()
	t = DoubleRobotChooseMeasurements( LogClass.Logger(), None )
	print( t.select_measurements() )
