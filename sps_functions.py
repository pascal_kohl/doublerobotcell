# -*- coding: utf-8 -*-
""" Modul: sps_functions
	Steuersript für SPS Funktionan an der topometric BMW-Stöhr Anlage
	Autor:     Pascal Kohl, Topometric
			   Florian Schinkel, Topometric
	
	Version:    	V3
	GOM-Version:	V8SR1
	Vorgänger:    -
	Erstellt:    2015-12-16
	letzte Änderung: 2016-12-05:
		- Fehler bei Handshake werden abgefangen

"""
import time

import s7_sps_handler as sps
import _config_.ini_reader as ini_reader
import _config_.pickle_reader as pickle_reader

class sps_functions():
	""" Klasse mit Funktionen zum erleichterten Steuern der Schnittstelle
	"""
	def __init__(self):
		""" Lege die Handlerklass aus 's7_sps_handler' an.
		"""
		self.my_sps = sps.sps_handler() # Start in Client Mode = Standard
		""" lese die Projektspezifischen ENUM_OUT ein """
		sps_parameter = ini_reader.ini_reader('_config_/sps_parameter.ini').import_values_to_dict()
		#self.ENUM_OUT = pickle_reader.pickle_reader(sps_parameter['ENUM_OUT']).import_values_to_dict()
		
		""" global VAR """
		self.TIMEOUT = 200 # Default: 200
		
		""" HANDLER """
		#self.DIALOG_TEACHING_MODE = DIALOGS().DIALOG_TEACHING_MODE #1.
		#self.DIALOG_TEACHING_MODE.handler = self.DIALOG_TEACHING_MODE_HANDLER
		
	def receive_sps(self):
		self.my_sps.open_socket_receiver()
		self.my_sps.receive_full_data_by_socket() # Empfange Daten
		#self.my_sps.close_socket_receiver()
		
	def send_sps(self):
		self.my_sps.open_socket_sender()
		self.my_sps.send_full_data_by_socket() # Empfange Daten
		
	
	def get_bit(self, BIT_NAME):
		return (self.my_sps.output_data[BIT_NAME][0]) # '0b0 = Bitwert 0, 0b1 = Bitwert 1
	

	
	def send_ENUM_command(self, ENUM):
		""" Sende ENUM Kommando an SPS,
			führe Handshake mit SPS durch: 2=sps_activ, 3=sps_finished
			--- Wird grade überarbeitet. Signale ändern sich.
		"""
		self.ENUM = ENUM
		
		
		#print ('send ENUM.out: %s' %(self.my_sps.get_value('ENUM.out')))

		self.receive_sps()


		TIMEOUT = 0
		print ('sps bereit? = %s' %(self.get_bit('6.6')))
		while self.get_bit('6.6') == 0b0 and TIMEOUT <= 200: # SPS bereit bei 0b1

			TIMEOUT += 1
			self.receive_sps()
			
		self.my_sps.set_value('ENUM.out', self.ENUM) # Ändere das ENUM.in Byte	
		self.send_sps() # Sende Daten
			
		
		""" Handshake zu SPS """
		if self.handshake_to_sps() == True:
			HANDSHAKE = True
		else:
			HANDSHAKE = False
			
		return HANDSHAKE
				
	def handshake_to_sps(self):
		""" Empfangshandshake """
		self.delay = 0.1 # Delayzeit Variable
		self.receive_sps()

		
		TIMER = 0
		""" Datenempfangsschleife """
		""" warte ab, solange die Werte noch 0 sind """
		while self.my_sps.get_value('ENUM.in') == 0 and TIMER < self.TIMEOUT:
			print ('SPS should start its work... Durchlauf %i'%(TIMER))
			print ('ENUM.in = %s' %(self.my_sps.get_value('ENUM.in')))
			TIMER += 1
			time.sleep(self.delay)
			self.receive_sps()
			
			""" Testprints """
			print ('Byte 0: %s %s %s %s %s %s %s %s' %(self.my_sps.output_data['0.0'][0],self.my_sps.output_data['0.1'][0],\
					self.my_sps.output_data['0.2'][0],self.my_sps.output_data['0.3'][0], self.my_sps.output_data['0.4'][0], self.my_sps.output_data['0.5'][0], \
					self.my_sps.output_data['0.6'][0],self.my_sps.output_data['0.7'][0]))
			print ('Byte 1: %s %s %s %s %s %s %s %s' %(self.my_sps.output_data['1.0'][0],self.my_sps.output_data['1.1'][0],\
					self.my_sps.output_data['1.2'][0],self.my_sps.output_data['1.3'][0], self.my_sps.output_data['1.4'][0], self.my_sps.output_data['1.5'][0], \
					self.my_sps.output_data['1.6'][0],self.my_sps.output_data['1.7'][0]))
		
			""" Schleife läuft, solange die SPS Daten !=0 sendet und
			der ENUM_check noch nicht bestätigt wurde. """
		if TIMER >= self.TIMEOUT:
				print('Handshake nicht korrekt abgelaufen!')
				self.my_sps.set_value('ENUM.out', 0) # Wenn Handshake fertig ist, ENUM.out zurück auf 0
				self.send_sps() # Sende Daten
				return False
		else:
			TIMER = 0
		ARBEITSTIMER = 0
		ARBEITSTIMEOUT = 40
		my_run_flags = [True, False, False] # Anpassung für BMW FIZ
		while False in my_run_flags and TIMER < self.TIMEOUT and ARBEITSTIMER < ARBEITSTIMEOUT:
			self.receive_sps()
			
			print ('ENUM.in = %s' %(self.my_sps.get_value('ENUM.in')))
			if self.my_sps.get_value('ENUM.in') == self.ENUM:
				""" Prüfe ENUM """
				print ('ENUM korrekt verstanden, bestätige...')
				#my_run_flags[0] = True
				self.my_sps.set_value('ENUM.out', 1) # Ändere das ENUM.out auf 1 als Bestätigung
				self.send_sps() # Sende Daten
			

			elif self.my_sps.get_value('ENUM.in') == 2:
				""" SPS in ACK """
				print ('SPS arbeitet, bitte warten... ')
				TIMER = 0
				ARBEITSTIMER += 1
				self.my_sps.set_value('ENUM.out', 2) # Ändere das ENUM.out auf 1 als Bestätigung
				self.send_sps() # Sende Daten
				time.sleep(self.delay)
				
			elif self.my_sps.get_value('ENUM.in') == 3:
				""" SPS fertig """
				print ('SPS fertig')
				self.my_sps.set_value('ENUM.out', 0) # Wenn Handshake fertig ist, ENUM.out zurück auf 0
				self.send_sps() # Sende Daten
				my_run_flags[1] = True
				
			elif self.my_sps.get_value('ENUM.in') == 0 and my_run_flags[0] == True and my_run_flags[1] == True:
				""" Handshake fertig und SPS hat wieder ENUM 0 """
				print ('Handshake korrekt abgeschlossen')
				my_run_flags[2] = True
			elif self.my_sps.get_value('ENUM.in') > 3:
				print('SPS hat einen ungültigen Wert geliefert!')
				self.my_sps.set_value('ENUM.out', 0) 
				self.send_sps()
				return False
			
			time.sleep(self.delay)
			TIMER += 1
			
			print('Timer:', TIMER)
			print('Arbeitstimer:', ARBEITSTIMER)
			
			if False not in my_run_flags:
				""" Handshake ok, Return True """
				self.my_sps.set_value('ENUM.out', 0) # Wenn Handshake fertig ist, ENUM.out zurück auf 0
				self.send_sps() # Sende Daten
				return True
		print('Handshake nicht korrekt abgelaufen!')
		self.my_sps.set_value('ENUM.out', 0) # Wenn Handshake fertig ist, ENUM.out zurück auf 0
		self.send_sps() # Sende Daten
		return False
		
				
	def send_ENUM_command_offline_tester(self, ENUM):
		""" Sende ENUM Kommando an print
		"""
		print ('ENUM empfangen: %s' %(ENUM))
		for i in range(5):
			#time.sleep(1)
	
			print ('DHE wird zur Messung bereit gemacht, %s' %(i))

		return True
		
	def my_signals(self):
		""" Die Signale der entsprechenden Reihe müssen alle
			0b1 also True sein, sonst darf die Anlage nicht starte
		"""
		self.MY_SIGNALS = [['3.4','3.5', '3.6', '3.7', '4.0', '6.2', '6.3', '6.6', '5.4']] # Justagereferenzierung = AB00
		self.MY_SIGNALS.append(['3.4', '3.6', '3.7', '4.0', '6.2', '6.3', '6.6', '7.0']) # Reihe 1 = AB01
		self.MY_SIGNALS.append(['3.4', '3.6', '3.7', '4.0', '6.2', '6.3', '6.6', '7.0']) # Reihe 2 = AB02
		self.MY_SIGNALS.append(['3.5', '3.6', '3.7', '4.0', '6.2', '6.3', '6.6', '6.7']) # Reihe 3 = AB03
		self.MY_SIGNALS.append(['3.5', '3.6', '3.7', '4.0', '6.2', '6.3', '6.6', '6.7']) # Reihe 4 = AB04
		
		
		
		return self.MY_SIGNALS
		
	def check_my_signals(self, MY_SIGNALS, WA):
		CHECKS = [False]
		ROBO_JUST = False
		print ('WA: %s' %(WA))
		while 0b0 in CHECKS or False in CHECKS or ROBO_JUST==True:
			CHECKS = []
			self.receive_sps()

			ROBO_JUST = self.get_bit('6.5')
			#ROBO_JUST = 0b1
			if ROBO_JUST:
				WA = 0
				self.FEHLERTEXT = '<br><br><b>Der Roboter hat eine Justagereferenzierung angefordert.<br> \
				Bitte prüfen Sie die Anlage, alle Signale müssen OK sein.<br> \
				Danach wird der Roboter eine Justagereferenzierung durchführen.</b><br><br>\
				<i>Hinweis: Die Anzeige aktualisiert sich, wenn Sie auf OK klicken.<br>\
				Sind alle Signale OK, wird der Roboter bei Klick auf den OK-Button starten.'
			else:
				self.FEHLERTEXT = '<br><br><b>Bitte Anlage Betriebsbereit machen, dann weiter mit OK</b>'

				
			for i in MY_SIGNALS[WA]:
				print (i)
				CHECKS.append(self.get_bit(i))
			
			""" Justagereferenzierung starten """
			if ROBO_JUST and 0b0 not in CHECKS:
				self.send_ENUM_command(22) # Sende Justageanforderung an SPS
			
			

			
			
			
	def error_message_for_signals(self, MY_SIGNALS):
		#self.FEHLERTEXT = '<br><br><b>Bitte Anlage Betriebsbereit machen, dann weiter mit OK</b>'
		HTML_TEXT = '<html><body><font size=4><table border=1>'
		for i in MY_SIGNALS:
			if self.my_sps.output_data[i][0] == 1:
				color = '#55AA55'
				value = 'OK'
			else:
				color = '#AA5555'
				value = 'nOK'
			HTML_TEXT += '<tr><td width=300>%s</td><td bgcolor=%s width=60 align=center>%s</td></tr>' %(self.my_sps.output_data[i][1], color, value)
		HTML_TEXT += '</table>%s</font></body></html>' %(self.FEHLERTEXT)
		
		return HTML_TEXT

		
if __name__ == '__main__':
	f = sps_functions()
	f.send_ENUM_command(9) # ENUM senden, Handshake


	
