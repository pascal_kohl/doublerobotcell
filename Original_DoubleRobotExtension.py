# -*- coding: utf-8 -*-
# Script: DoubleRobot Extension file
#
# PLEASE NOTE that this file is part of the GOM Inspect Professional software
# You are not allowed to distribute this file to a third party without written notice.
#
# Copyright (c) 2013 GOM Gesellschaft fuer optische Messtechnik mbH
# Author: GOM Software Development Team (M.V.)
# All rights reserved.

# GOM-Script-Version: 7.7
#

import gom

from KioskInterface.Base.Misc import Utils, Globals

def on_finished_measuring( self, reason ):
	'''
	called directly after the last selected measurement series was performed
	'''
	if reason == 'atos':
		pass
	elif reason == 'tritop':
		pass


def after_evaluation( self, reason ):
	'''
	called directly before the current project gets closed on an successfull evaluation
	'''
	if reason == 'atos':
		pass
	elif reason == 'tritop':
		pass

def before_tritop_export( self, photogrammetry_series ):
	'''
	called directly before export as refxml

	return True if export should be done, else False

	SavePath should be
	os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_RefXML_Path )
	 if export here
	'''
	return True

def before_use_reference_points( self, measurement_series ):
	'''
	current Implementation:
	gom.script.atos.use_external_reference_points (
								file = os.path.join( Globals.SETTINGS.DoubleRobot_TransferPath, Globals.SETTINGS.DoubleRobot_RefXML_Path ),
								gsi_file_unit = 'mm',
								load_ascii_as_gsi = False,
								measurement_series = measurement_series )
	return True if import should be done, else False
	'''
	return True

def before_poly_xml_export( self ):
	'''
	called directly before polygonization and export of the xml

	return True if poly and export should be done, else False

	FileStorage should be (if done here):
	gom.script.sys.export_gom_xml (
				elements = [e for e in gom.ElementSelection ( {'category': ['key', 'elements', 'overview_explorer_categories', 'all_elements', 'explorer_category', 'actual']} )
							if str( e.get( 'object_family' ) ) != 'measurement_series'],
				file = os.path.join( Globals.SETTINGS.SavePath, Globals.SETTINGS.DoubleRobot_AtosXMLPath, '{}.xml'.format( gom.app.project.get( 'name' ) ) ),
				format = gom.File ( 'giefv20.xsl' ) )
	'''
	return True

