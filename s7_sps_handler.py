# -*- coding: utf-8 -*-
""" Modul: sps_handler --> zum steuern der SPS via Socket
	beinhaltet Class data_handler() zum Verarbeiten der SPS-Datastreams
	Autor: 	Pascal Kohl
	
	Version: 	V0.5
	Vorgänger:	data_handler
	Erstellt:	2014-11-03
	letzte Änderung: 2015-11-06:
		- Bugfix im Encoding
		- Ergänzung Server-Funktionalität
		- Komfortfunktionen ergänzt. Einfaches Wertelesen und Ändern.
		
"""

#import gom
import sys, os, time
import socket, socketserver
#from Codebeispiele.Datentypen.bit import my_byte


class sps_handler():
	""" Data handler class
		Klasse zum Verarbeiten der Schnittstelle zwischen GOM und SPS.
		Schreiben von Bytestreams auf Variablen
		und umgekehrt.
		Das Verbindungsprinzip sind 2 geöffnete Datenports. Einer zum lesen,
		der andere zum schreiben. Die Verbindungen sollten geöffnet bleiben,
		solange der Handler verwendet wird.
	"""
	def __init__(self, mode='client', input_data=None):
		""" INIT
			Es wird ein "default_data" vom Typ Dict erwartet: {STRING: [BYTE, STRING]} erwartet.
			Wird keins übergeben, wird ein Default aus der Datei sps_dict.pep geladen
		"""
		## Var ##
		#from _sps_vars_ import HOST, PORT, PORT_SENDER, PORT_RECEIVER
		""" SPS-Variablen werden aus der _config_/_parameter.ini geladen """
		import _config_.pickle_reader as pickle_reader
		import _config_.ini_reader as ini_reader
		sps_parameter = ini_reader.ini_reader('_config_/sps_parameter.ini').import_values_to_dict()
		
		""" Zuordnung in die Klassenvariablen """
		self.HOST = sps_parameter['HOST']
		self.PORT_SENDER = int(sps_parameter['PORT_SENDER'])
		self.PORT_RECEIVER = int(sps_parameter['PORT_RECEIVER'])
		
		self.TIMER = float(sps_parameter['TIMER'])
		self.TIMEOUT = float(sps_parameter['TIMEOUT'])
		self.SOCKET_BUFFER = int(sps_parameter['SOCKET_BUFFER'])
		
		""" Hole SPS_DICT und SPS_DEFINITION """
		if input_data == None:
			self.output_data = pickle_reader.pickle_reader(sps_parameter['SPS_DICT_FILE']).import_values_to_dict()
		else:
			self.output_data = input_data
		self.sps_definition = pickle_reader.pickle_reader(sps_parameter['SPS_DEFINITION_FILE']).import_values_to_dict()
		
		
		# Importiere Kodierung für den Datenstream aus Modul #
		if mode == 'client':
			""" Bei 'client' bleiben Coding und Encoding in der urspruenglichen Reihenfolge
				--> d.h. aus Sicht des Clients!
			"""
			self.stream_coding = pickle_reader.pickle_reader(sps_parameter['STREAM_CODING_FILE']).import_values_to_dict()
			self.stream_encoding = pickle_reader.pickle_reader(sps_parameter['STREAM_ENCODING_FILE']).import_values_to_dict()
			
		elif mode == 'server' or mode == 'host':
			""" Bei 'server' werden Coding und Encoding vertauscht, damit sich die Codierungsrichtung
				umkehrt --> d.h. wir drehen die Urspuengliche Sicht von Client herum
			"""
			self.stream_coding = pickle_reader.pickle_reader(sps_parameter['STREAM_CODING_FILE']).import_values_to_dict()
			self.stream_encoding = pickle_reader.pickle_reader(sps_parameter['STREAM_ENCODING_FILE']).import_values_to_dict()
			self.act_as_socket_server()
			
	def act_as_socket_server(self):
		""" Socket Verbindung als Server durchführen.
		"""
		pass
		print ('no default Connection in Servermode')
		
# 		print ('### SELF: \n', self, type(self))
# 		self.server = socketserver.TCPServer((self.HOST, self.PORT), MyTCPHandler, self)
# 		print ('\n### I am a Socket Server on: %s:%i ###' %(self.HOST,self.PORT))
# 		print('### I am a SPS_Handler V0.1 ###\n')
# 		self.server.serve_forever()
	

	def open_socket_sender(self):
		""" Socket Sender öffnen """
		print('opening SENDER to HOST: %s, PORT: %i' %(self.HOST, self.PORT_SENDER))
		self.sender = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sender.connect((self.HOST, self.PORT_SENDER))
	
	def close_socket_sender(self):
		self.sender.close()
		print('SENDER closed')
		
	def open_socket_receiver(self):
		""" Socket Receiver öffnen """
		print('opening RECEIVER to HOST: %s, PORT: %i' %(self.HOST, self.PORT_RECEIVER))
		self.receiver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.receiver.connect((self.HOST, self.PORT_RECEIVER))
		
	def close_socket_receiver(self):
		self.receiver.close()
		print('RECEIVER closed')
		
	def send_full_data_by_socket(self):
		""" Daten auf bestehender Socket-Verbindung senden.
			ENUM und weitere Änderungen an "self.output_data" werden an SPS gesendet
		"""
		if not hasattr(self, 'sender'):
			print('open Socket SENDER...')
			self.open_socket_sender()
			print('Socket SENDER online...')
		else:
			print('Sockets always connected.')
			
		out_stream = (self.dict_to_bytestream())
		self.sender.sendall(out_stream)

	def receive_full_data_by_socket(self):
		""" Daten auf bestehender Socket-Verbindung empfangen.
			ENUM und weitere Änderungen an "self.output_data" werden empfangen
		"""
		if not hasattr(self, 'receiver'):
			print('open Socket RECEIVER...')
			self.open_socket_receiver()
			print('Socket RECEIVER online...')
		else:
			print('Sockets always connected.')
		
		data = self.receiver.recv(1024)
		
		if data:
			self.bytestream_to_dict(data)
		else:
			print('no Data received!')

	
	def bytestream_to_dict(self, byte_stream):
		""" Dict mit den Daten fordert folgende Keys im STRING-Format:
			z.B. "0.0", "0.1", ... "8.0", "8.1", ... , "8.7"
			Erste Stelle bezeichnet das BYTE, die Nachkommastelle das BIT
		"""
		#print (byte_stream)
# 		print('len bytestream i: %i' %(len(byte_stream)))
# 		print('len ENCODING %i' %(len(self.stream_encoding)))
		
# 		for i in range(len(byte_stream)):
# 			print('bytestream i: %s, i: %s' %(byte_stream[i],self.stream_encoding[i][0]))
# 
		#print(byte_stream)
		for i in range(len(byte_stream)):
			self.int_to_dict_byte(byte_stream[i],[self.stream_encoding[i][0]])
				
	def dict_to_bytestream(self):
		""" Schreibe das Dict in einen bytearray.
			Dabei wird die Kodierung aus der _var_ stream_coding verwendet
		"""
		#print(self.stream_coding)
		out_stream = bytearray()
		for i in self.stream_coding:
			#print ('self.stream_coding var: %s'%(i))
			my_int = self.dict_byte_to_int([i[0]]) # einfache Form, nur ein Byte. Ändern bei 2 Byte
			out_stream.append(my_int)
		#print (out_stream)
		return (out_stream)
		
	def dict_byte_to_int(self, byte_list):
		""" Funktion zum decodieren bestimmter Bytes im ENUM-Prinzip.
			bytes_to_enum(list), Funktion fordert eine List mit den zu decodierenden Bytenummern
			z.B. [7, 8] oder [12, 13] oder [5].
			Return ist die decodierte Integer-Zahl.
		"""
		#print (byte_list)
		my_byte = (bytearray(len(byte_list)*8)) # Lege festen Byte mit "8-BIT" an
		z = 0 # Zähler über die beiden Schleifen hinaus weiterzählend
		for i in range(len(byte_list)):
			for u in range(8):
				key = str(byte_list[i]) + '.' + str(u)
				#print (key, self.output_data[key][0])
				my_byte[z] = self.output_data[key][0]
				z += 1
		#print (my_byte)
		# Nur für formatierte print #
		my_print_string = ''
		for i in range(len(my_byte)):
			my_print_string += str(my_byte[i])
		#print (my_print_string)
		# --
		
		my_value = self.bytearray_to_int(my_byte) # schiebe Bytearray in die Dekodierungsfunktion 'bytearray_to_int(bytearray())'
		return (my_value) # my_enum = dekodiertes Byte = Integer
	
	
	def int_to_dict_byte(self, my_int, byte_list):
		""" Funktion schreibt eine Integerzahl in das Dict.
			int_to_dict_byte(Integerzahl, Liste mit den zu schreibenen Bytes z.B. [24,25])
			Das Dict besteht aus Bits, also erfolgt erst die Kodierung, dann wird bitweise geschrieben
		"""
		# Ausgabe
		#print ('Wert: ' + str(my_int) + ' Listenwert: '+ str(byte_list))
		
		arraysize = (len(byte_list)*8)
		my_byte = self.int_to_bytearray(my_int, arraysize)
		z = 0 # ZÃ¤hler Ã¼ber die beiden Schleifen hinaus weiterzÃ¤hlend
		for i in range(len(byte_list)):
			for u in range(8):
				key = str(byte_list[i]) + '.' + str(u)
				#print ('key: ' + str(key) + ' Wert: ' + str(my_byte[z]))
				self.output_data[key][0] = my_byte[z]
				#print (self.output_data[key][0])
				z += 1

	
	def int_to_bytearray(self, my_int, arraysize):
		""" Funktion wandelt eine Integerzahl in ein Bytearray
			Die Größe des Bytearrays muss in Bit als Integerzahl angegeben werden
			int_to_bytearray(my_int, arraysize):
				my_int = Integer zum umwandeln
				arraysize = Größe des Bytearrays in Bit
		"""
		if my_int >= (2** arraysize):
			raise Exception ('Zahl zu Groß für die Byteanzahl!')
			return None
		else:
			self.my_int = my_int
			self.arraysize = arraysize
			
			self.__my_byte = bytearray(self.arraysize)
			self.my_bin = bin(self.my_int) # Schreibe im binary als String
			#print (my_bin)
			
			# STRING OPERATIONEN hier
			self.my_bin = (self.my_bin[2:len(self.my_bin)]) # lösche das beginnende "0b" 
			self.my_bin = self.my_bin.rjust(self.arraysize,'0') # fülle auf die definierte Größe mit "0" auf
			#print (self.my_bin)
			self.my_bin = self.my_bin[::-1]
			#print (self.my_bin)
			for i in range(self.arraysize):
				if self.my_bin[i] == '1':
					self.__my_byte[i] = True
				else:
					self.__my_byte[i] = False
			return (self.__my_byte)

	def bytearray_to_int(self, my_bytearray):
		""" Funktion wandelt ein beliebig langes Bytearray zurück in eine Integer
			bytearray_to_int(my_bytearray)):
			my_bytearray = Bytearray
		"""
		my_bin = ''
		for i in range(len(my_bytearray)):
			my_bin += str(my_bytearray[i])
		
		my_bin = my_bin[::-1]
		my_int = int(my_bin, 2)
		return (my_int)

	def get_value(self, value_name):
		""" Hole Werte nach Name,
			Vorzeichen werden ebenfalls aus den VZ-Bytes gesetzt
		"""
		""" Suche nach Bytes und verwende diese um die INT-Zahl
			zurückzugeben
		"""
		my_findings = []
		for i in self.sps_definition:
			if value_name == (self.sps_definition[i][0]):
				my_findings.append(i)
		
		""" Nur wenn my_findings eine Ergebnisliste liefert, wird hier weiter ausgeführt.
			Ansonsten return None
		"""
		if len(my_findings) != 0:
			my_value = self.dict_byte_to_int(my_findings) # Decodierung der Bytes zu INT
			""" Prüfe ob es eine Verknüpfung zu Vorzeichen-Byte gibt.
				Wenn ja, versehe Zahl mit Vorzeichen: *(-1)
			"""
			sign_byte = []
			for i in my_findings:
				if self.sps_definition[i][4] != None:
					sign_byte.append(self.sps_definition[i][4])
			if len(sign_byte) == 1:
				if self.dict_byte_to_int(sign_byte) == 0:
					my_value = my_value *(-1)
				elif self.dict_byte_to_int(sign_byte) > 1:
					raise Exception('Vorzeichenfehler! Das Vorzeichen-Byte darf nur 0=negativ oder 1=positiv sein.')
			elif len(sign_byte) > 1:
				print ('mehrere Vorzeicheneinträge gefunden. Vergleichen ob sie gleich sind...')
		
		else:
			my_value = None
				
		return my_value
	
	def set_value(self, value_name, value):
		""" Hole Werte nach Name,
			Vorzeichen werden ebenfalls aus den VZ-Bytes gesetzt
		"""
		""" Suche nach Bytes und verwende diese um die INT-Zahl
			zurückzugeben
		"""
		my_findings = []
		for i in self.sps_definition:
			if value_name == (self.sps_definition[i][0]):
				my_findings.append(i)
		#print(my_findings)
		
		""" Wenn kein Vorzeichen, d.h. value >= 0, dann einfach mit bekannter Funktion
			ins Dict schreiben.
			Wenn Vorzeichen, dann Vorzeichen-Byte suchen und setzen
		"""
		#print (my_findings)
		sign_byte = []
		for i in my_findings:
			if self.sps_definition[i][4] != None:
				sign_byte.append(self.sps_definition[i][4])
		if len(sign_byte) == 0 and value < 0:
			raise Exception('Datenfehler. Variable ist nicht für negative Werte vorgesehen!')
		elif len(sign_byte) == 0 and value >= 0:
			self.int_to_dict_byte(value, my_findings)
		elif len(sign_byte) == 1:
			if value < 0:
				self.int_to_dict_byte(0, sign_byte)
				self.int_to_dict_byte(value*(-1), my_findings)
			else:
				self.int_to_dict_byte(1, sign_byte)
				self.int_to_dict_byte(value, my_findings)

		elif len(sign_byte) > 1:
			print ('mehrere Vorzeicheneinträge gefunden. Vergleichen ob sie gleich sind...')
			
	


class MyTCPHandler_receiver(socketserver.BaseRequestHandler, sps_handler):
	""" Handler für den DEMO-RECEIVER. Wird zum lokalen testen der Verbindung benötigt.
	"""
	def handle(self):
		os.system('cls')
		self.sps_server = sps_handler('host')
		#print ('### SELF: \n', sps_handler, type(sps_handler))
		while True:
			print ('-Client connected-')
			self.data = self.request.recv(1024)
			self.sps_server.bytestream_to_dict(self.data)
			
# 			for i in range(len(self.data)):
# 				print ('Stream Value %i: %s' %(i, self.data[i]))
				
			"""Zahlenwerte ausgeben"""
			print ('Value 1 (12,13,14,15): %s'%(self.sps_server.dict_byte_to_int([12,13,14,15]))) # Wert 1 Byte 12,13,14,15
			print ('Value 2 (16,17,18,19): %s'%(self.sps_server.dict_byte_to_int([16,17,18,19])))
			print ('Value 3 (20,21,22,23): %s'%(self.sps_server.dict_byte_to_int([20,21,22,23])))
			print ('Value 4 (24,25,26,27): %s'%(self.sps_server.dict_byte_to_int([24,25,26,27])))
			print ('Value 5 (28,29,30,31): %s'%(self.sps_server.dict_byte_to_int([28,29,30,31])))
			print ('Value 6 (32,33,34,35): %s'%(self.sps_server.dict_byte_to_int([32,33,34,35])))
			print ('Value 7 (36,37,38,39): %s'%(self.sps_server.dict_byte_to_int([36,37,38,39])))
			print ('Value 8 (40,41,42,43): %s'%(self.sps_server.dict_byte_to_int([40,41,42,43])))
			print ('Value 9 (44,45,46,47): %s'%(self.sps_server.dict_byte_to_int([44,45,46,47])))
			print ('Value 10(48,49,50,51): %s'%(self.sps_server.dict_byte_to_int([48,49,50,51])))
			""" passende Vorzeichen zu Zahlenwerten """
			print('VZ 1: %i' %(self.sps_server.dict_byte_to_int([52])))
			print('VZ 2: %i' %(self.sps_server.dict_byte_to_int([53])))
			print('VZ 3: %i' %(self.sps_server.dict_byte_to_int([54])))
			print('VZ 4: %i' %(self.sps_server.dict_byte_to_int([55])))
			print('VZ 5: %i' %(self.sps_server.dict_byte_to_int([56])))
			print('VZ 6: %i' %(self.sps_server.dict_byte_to_int([57])))
			print('VZ 7: %i' %(self.sps_server.dict_byte_to_int([58])))
			print('VZ 8: %i' %(self.sps_server.dict_byte_to_int([59])))
			print('VZ 9: %i' %(self.sps_server.dict_byte_to_int([60])))
			print('VZ 10: %i' %(self.sps_server.dict_byte_to_int([61])))
			
			
			#self.sps_server.int_to_dict_byte(123456789, [9,10,11,12])

			if not self.data:
				print('DISCONNECTED')
				break
			#print('\nRECEIVED Stream: %s \n' %(self.data))
			#print('Received SPS-Values:')
			
# 			""" Testprint """
# 			for i in range(int(len(self.sps_server.output_data)/8)):
# 				print ('Byte %i, INT: %s, Bit: %s '%(i, self.sps_server.dict_byte_to_int([i]), bin(self.sps_server.dict_byte_to_int([i]))[2:].zfill(8)))
			
class MyTCPHandler_sender(socketserver.BaseRequestHandler, sps_handler):
	""" Handler für den DEMO-SENDER. Wird zum lokalen testen der Verbindung benötigt.
	"""
	def handle(self):
		""" Zufallswerte erzeugen? Dann randomize = True """
		randomize = True
		
		os.system('cls')
		self.sps_server = sps_handler('host')
		#print ('### SELF: \n', sps_handler, type(sps_handler))
		import time
		while True:
			print ('-server connected-')
			""" Umbau zu Dauersender"""
			#self.request.sendall((self.data))
			print('sending Data:')
			if randomize:
				from random import randint
				for i in range(int(len(self.sps_server.output_data)/8)-10): # -10, da die letzen 10 Werte Vorzeichen für die Parameter sind.
					self.sps_server.int_to_dict_byte(randint(0,255), [i])
					print(self.sps_server.dict_byte_to_int([i]))
				for i in range((int(len(self.sps_server.output_data)/8)-10),(int(len(self.sps_server.output_data)/8))):
					self.sps_server.int_to_dict_byte(randint(0,1), [i])
					print(self.sps_server.dict_byte_to_int([i]))
			else:
				""" sende Werte 0-X. Bytenummer = Wert """
				for i in range(int(len(self.sps_server.output_data)/8)-10): # -10, da die letzen 10 Werte Vorzeichen für die Parameter sind.
					self.sps_server.int_to_dict_byte(i, [i])
					print(self.sps_server.dict_byte_to_int([i]))

				
			out_stream = (self.sps_server.dict_to_bytestream())
			print(out_stream)
			#self.sendall(out_stream)
			self.request.sendall(out_stream)
			
			time.sleep(1)
					
class sps_handler_as_server_receiver():
	""" DEMO-RECEIVER, wird zum testen der Verbindung benötigt.
		Diese Klasse startet einen Socket-Server als RECEIVER, der Daten empfängt
		und in der Konsole anzeigt.
	"""
	def __init__(self):
		## Var ##
		import _config_.ini_reader as ini_reader
		sps_parameter = ini_reader.ini_reader('_config_/_parameter.ini').import_values_to_dict()
		
		""" Zuordnung in die Klassenvariablen """
		self.HOST = sps_parameter['HOST']
		self.PORT_SENDER = int(sps_parameter['PORT_SENDER'])
		self.PORT_RECEIVER = int(sps_parameter['PORT_RECEIVER'])
		
		self.TIMER = float(sps_parameter['TIMER'])
		self.TIMEOUT = float(sps_parameter['TIMEOUT'])
		self.SOCKET_BUFFER = int(sps_parameter['SOCKET_BUFFER'])
		
		## Functionality ##
		self.server = socketserver.TCPServer((self.HOST, self.PORT_SENDER), MyTCPHandler_receiver)
		print ('\n### I am a Socket Server RECEIVER on: %s:%i ###' %(self.HOST,self.PORT_SENDER))
		print('### I am a SPS_Handler V1.13 ###\n')
		self.server.serve_forever()

class sps_handler_as_server_sender():
	""" DEMO-SENDER, wird zum testen der Verbindung benötigt.
		Diese Klasse startet einen Socket-Server als SENDER, der Daten sendet
		sobald der Client die Verbindung aufgebaut hat.
	"""
	def __init__(self):
		## Var ##
		import _config_.ini_reader as ini_reader
		sps_parameter = ini_reader.ini_reader('_config_/_parameter.ini').import_values_to_dict()
		
		""" Zuordnung in die Klassenvariablen """
		self.HOST = sps_parameter['HOST']
		self.PORT_SENDER = int(sps_parameter['PORT_SENDER'])
		self.PORT_RECEIVER = int(sps_parameter['PORT_RECEIVER'])
		
		self.TIMER = float(sps_parameter['TIMER'])
		self.TIMEOUT = float(sps_parameter['TIMEOUT'])
		self.SOCKET_BUFFER = int(sps_parameter['SOCKET_BUFFER'])
		
		## Functionality ##
		self.server = socketserver.TCPServer((self.HOST, self.PORT_RECEIVER), MyTCPHandler_sender)
		print ('\n### I am a Socket Server SENDER on: %s:%i ###' %(self.HOST,self.PORT_RECEIVER))
		print('### I am a SPS_Handler V1.13 ###\n')
		self.server.serve_forever()

if __name__ == '__main__':
	pass
	#print (help(sps_handler)) # gibt den Hilfetext und die Funktionen der Klasse aus.
	
	#sps_handler_as_server()
	
	my_sps = sps_handler('client')

# 	""" GET TESTER """
# 	my_sps.int_to_dict_byte(25252525, [12,13,14,15])
# 	my_sps.int_to_dict_byte(1, [52])
# 	print (my_sps.get_value('PARA1'))
	
	""" SET TESTER """
	my_sps.set_value('PARA1', -35353535)
	print(my_sps.get_value('PARA1'))
	my_sps.set_value('PARA2', 35353535)
	print(my_sps.get_value('PARA2'))
	my_sps.set_value('ENUM.in', 111)
	print(my_sps.get_value('ENUM.in'))

	
	print (my_sps.get_value('PARA1'))

	
