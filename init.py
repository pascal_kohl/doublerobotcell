# -*- coding: utf-8 -*-
""" Modul: Init
	Die Intitialisierung dieses Moduls muss immer dann durchgefuehrt werden,
	wenn Multiprocesses verwendet werden sollen.
	Autor:     Florian Schinkel, Topometric
	
	Version:        V1
	GOM-Version:    V2016
	Vorgänger:    -
	Erstellt:    2016-12-05
"""
import gom
import os
import sys
import multiprocessing


def init():
	# Initiate:
	sys.argv = [os.path.join(gom.app.get ('public_directory'),'gom_scripts','PLCKiosk','KioskInterface','Base','Communication','Inline', 'InlineSubWatch.py')]
	lib_dir = os.path.join(gom.app.get ('software_directory'),'lib')
	python_dir = [d for d in os.listdir(lib_dir) if os.path.isdir(os.path.join(lib_dir, d)) and d.lower().startswith('python')]
	if not len(python_dir) or len(python_dir)>1:
		raise Exception('no python library directory found!')
	os.environ["PYTHONHOME"] = os.path.join(lib_dir, python_dir[0]) # das ist das spannende das dafür sorgt das gom-python.exe ausführbar ist
	os.environ["PYTHONDONTWRITEBYTECODE"]="1" # diese pyc dateien nicht generieren
	python_exe = os.path.join(gom.app.get ('software_directory'),'bin','gom-python.exe')
	multiprocessing.set_executable(python_exe) # multiprocessing diese exe setzen
