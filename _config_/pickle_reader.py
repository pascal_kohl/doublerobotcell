# -*- coding: utf-8 -*-
""" Modul: sps_ini_reader
    beinhaltet Class ini_reader() einlesen von INI Parameterdateien
    Autor:     Pascal Kohl, Topometric
    
    Version:     V1.13
    Vorgänger:    -
    Erstellt:    2014-11-03
    letzte Änderung: 
        
"""
import os
import sys
import pickle

class pickle_reader():
    """ Klasse zum lesen einer INI Datei.
        Aktuell speziell für SPS_INI geschrieben
    """
    def __init__(self, path):
        """ Prüfe ob Datei vorhanden..."""
        self.path = sys.path[0] + os.sep + path
        if not os.path.isfile(self.path):
            raise Exception('Die Parameterdatei %s fehlt!' %(self.path))
        
        try:
            self.file = open(self.path, mode='rb')
        except:
            raise Exception('Fehler beim öffnen der Parameterdatei %s !'%(self.path))
    
    def import_values_to_dict(self):
        #try:
        self.vars = pickle.load(self.file)
        #except:
        #raise Exception ('\nEine der benoetigten Config-Dateien ist nicht vorhanden oder kann nicht gelesen werden!\n\nsps_coding.config, sps_encoding.config, sps_dict.config')
        self.file.close()
        return (self.vars)
       
    def print_vars_dict(self):
        # DICT sortieren nach Keys und ausgeben #
        from collections import OrderedDict
        sorted_dict = (OrderedDict(sorted(self.vars.items())))
        for k, v in sorted_dict.items():
            print(k, v)
    
    def print_vars_list(self):
        for i in self.vars:
            print(i)


if __name__ == '__main__':
    my_pickle = pickle_reader('tm_coding.config')
    pickle_value = my_pickle.import_values_to_dict()
    print(pickle_value)
    my_pickle.print_vars_list()
