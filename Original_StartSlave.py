# -*- coding: utf-8 -*-
# Script: DoubleRobot Slave start script
#
# PLEASE NOTE that this file is part of the GOM Inspect Professional software
# You are not allowed to distribute this file to a third party without written notice.
#
# Copyright (c) 2013 GOM Gesellschaft fuer optische Messtechnik mbH
# Author: GOM Software Development Team (M.V.)
# All rights reserved.

# GOM-Script-Version: 7.7
#

import sys
from KioskInterface.Base.Misc import Utils, Globals, Messages, PersistentSettings

from Base import DoubleRobotUtils, DoubleRobotSlave, DoubleRobotLocalization


import gom
import os

if __name__ == '__main__':
	Globals.LOCALIZATION = DoubleRobotLocalization.DoubleRobotLocalization()
	Globals.SETTINGS = DoubleRobotUtils.DoubleRobotSettings(should_create=False)
	Globals.SETTINGS.SavePath = Globals.SETTINGS.DoubleRobot_ClientSavePath
	Globals.SETTINGS.DoubleRobot_TransferPath = Globals.SETTINGS.DoubleRobot_ClientTransferPath
	if not os.path.exists( Globals.SETTINGS.SavePath ):
		os.makedirs( Globals.SETTINGS.SavePath )
	Globals.PERSISTENTSETTINGS = PersistentSettings.PersistentSettings( Globals.SETTINGS.SavePath )
	slave = DoubleRobotSlave.DoubleRobotSlave()
	res = slave.start()

