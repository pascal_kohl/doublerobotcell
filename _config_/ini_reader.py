# -*- coding: utf-8 -*-
""" Modul: sps_ini_reader
	beinhaltet Class ini_reader() einlesen von INI Parameterdateien
	Autor:     Pascal Kohl, Topometric
	
	Version:     V1.13
	Vorgänger:    -
	Erstellt:    2014-11-03
	letzte Änderung: 
		
"""
import os
import sys


class ini_reader():
	""" Klasse zum lesen einer INI Datei.
		Aktuell speziell für SPS_INI geschrieben
	"""
	def __init__(self, path):
		""" Prüfe ob Datei vorhanden..."""
		self.path = sys.path[0] + os.sep + path
		print (self.path)
		if not os.path.isfile(self.path):
			raise Exception('Die Parameterdatei %s fehlt!' %(self.path))
		
		try:
			self.file = open(self.path, mode='r')
		except:
			raise Exception('Fehler beim öffnen der Parameterdatei %s !'%(self.path))
	
	def import_values_to_dict(self):
		data = self.file.read().splitlines()
		self.vars = {'HOST': 'mylocal'}
		for i in data:
			#print(i)
			new_list = (i.strip().replace(' ', '').split('='))
			self.vars.update({new_list[0]: new_list[1]})
		self.file.close()
		return (self.vars)
		
	def print_vars_dict(self):
		# DICT sortieren nach Keys und ausgeben #
		from collections import OrderedDict
		sorted_dict = (OrderedDict(sorted(self.vars.items())))
		for k, v in sorted_dict.items():
			print(k, v)
		


if __name__ == '__main__':
	read = ini_reader('gui_parameter.ini')
	read.import_values_to_dict()
	read.print_vars_dict()
