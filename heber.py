# -*- coding: utf-8 -*-
""" Modul: heber
	Erweiterung fuer topometric BMW-Stoehr Anlage.
	Kommuniziere direkt mit der Hebebuehne ueber SPS.
	Erhaelt als Parameter eine Queue, in die
	Rueckgabenachrichten geschrieben werden koennen und
	einen Boolean, der steuert, ob die
	Buehne gehoben(True) oder gesenkt (False) werden soll.
	
	Benutzt dabei die Funktionen von sps_functions, um
	Dateien direkt an die SPS zu senden (ENUM 10 oder ENUM 11).
	
	Schreibt abschliessend in die Queue, ob der Vorgang erfolgreich (ACK)
	war oder nicht (NACK).
	Autor:     Florian Schinkel, Topometric
	
	Version:        V1
	GOM-Version:    V2016
	Vorgänger:    -
	Erstellt:    2016-12-05
"""
import sps_functions as sps  
import multiprocessing
	
def heben(q, hoch):
	print('Starte Heber')
	f = sps.sps_functions()
	aufgabe = 'Error'
	handshakeAccepted = False
	
	# Fuehre den Hebe- oder Senkbefehl aus:
	if hoch:
		aufgabe = 'heben'
		handshakeAccepted = f.send_ENUM_command(11)
		print('Sende ENUM 11')
	else:
		aufgabe = 'senken'
		handshakeAccepted = f.send_ENUM_command(10)
		print('Sende ENUM 10')
	
	# Sende eine Ruecknachricht, indem sie in die Queue geschrieben wird:
	if handshakeAccepted:
		q.put('ACK')
		print('Heber konnte ' + aufgabe + '!')
		
	else:
		q.put('NACK')
		print('Heber konnte nicht ' + aufgabe + ',da Handshake verweigert wurde!')
		
		
if __name__ == '__main__':
	q = multiprocessing.Queue()
	heben(q, False)
	q.put('Error')
	
