# -*- coding: utf-8 -*-
# Script: DoubleRobot Master start script
#
# PLEASE NOTE that this file is part of the GOM Inspect Professional software
# You are not allowed to distribute this file to a third party without written notice.
#
# Copyright (c) 2013 GOM Gesellschaft fuer optische Messtechnik mbH
# Author: GOM Software Development Team (M.V.)
# All rights reserved.

# GOM-Script-Version: 7.7
#

import gom
import os, sys

from KioskInterface.Base.Misc import Utils, Globals, Messages, PersistentSettings

from Base import DoubleRobotUtils, DoubleRobotMaster, DoubleRobotLocalization

def server_workflow():
	Globals.LOCALIZATION = DoubleRobotLocalization.DoubleRobotLocalization()
	Globals.SETTINGS = DoubleRobotUtils.DoubleRobotSettings()
	Globals.PERSISTENTSETTINGS = PersistentSettings.PersistentSettings( Globals.SETTINGS.SavePath )
	server = DoubleRobotMaster.DoubleRobotWorkflow()
	try:
		res = True
		while res:
			res = server.start()
	except Exception as e:
		server.log.exception( 'Unhandled Exception: {}'.format( e ) )
	finally:
		server.exit_handler()

if __name__ == '__main__':
	server_workflow()

